<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        Model::unguard();

        DB::table('users')->delete();

        $users = array(
                ['username'=>'ryan', 'name' => 'Ryan Chenkie', 'email' => 'ryanchenkie@gmail.com', 'password' => bcrypt('admin')],
                ['username'=>'chris', 'name' => 'Chris Sevilleja', 'email' => 'chris@scotch.io', 'password' => bcrypt('admin')],
                ['username'=>'holly', 'name' => 'Holly Lloyd', 'email' => 'holly@scotch.io', 'password' => bcrypt('admin')],
                ['username'=>'adna', 'name' => 'Adnan Kukic', 'email' => 'adnan@scotch.io', 'password' => bcrypt('admin')],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

        Model::reguard();
    }
}
