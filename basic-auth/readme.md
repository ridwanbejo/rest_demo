#Rest Demo

Laravel Restful API Demo powered with JWT Authentication.

Sample URL:
```
(POST) http://localhost/rest_demo/public/index.php/api/authenticate
(GET) http://localhost/rest_demo/public/index.php/api/authenticate/user?token=secret
(POST) http://localhost/rest_demo/public/index.php/api/register
(GET) http://localhost/rest_demo/public/index.php/api/task?token=secret
(POST) http://localhost/rest_demo/public/index.php/api/task?token=secret
(PUT) http://localhost/rest_demo/public/index.php/api/task/100?token=secret
(DELETE) http://localhost/rest_demo/public/index.php/api/task/100?token=secret
```