<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title','isDone'];
    
    public function user(){
 
        return $this->belongsTo('App\User');
 
    }
}
