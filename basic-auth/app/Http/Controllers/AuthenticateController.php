<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;

class AuthenticateController extends Controller
{
	public function __construct()
	{
		$this->middleware('simpleauth', ['except' => ['register']]);
	}

    public function index()
    {
        // TODO: show users

        $users = User::all();
    	return $users;
    } 

    public function register(Request $request){
 
        $newuser = $request->all();
        $password = bcrypt($request->input('password'));
 
        $newuser['password'] = $password;
 
        return User::create($newuser);
    }
}
