<?php

use App\Task;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'api'], function()
{
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::post('register', 'AuthenticateController@register');

	Route::get('task', ['uses' => 'TaskController@index']);
	Route::post('task', ['uses' => 'TaskController@store']);
	Route::put('task/{id}', ['uses' => 'TaskController@update']);
	Route::delete('task/{id}', ['uses' => 'TaskController@destroy']);
});