<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Task;

use Auth;
use JWTAuth;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function index() {
 
     	// $tasks = Task::where('user_id','=', Auth::user()->id )->get();

      $user = JWTAuth::parseToken()->authenticate();
      $tasks = Task::where('user_id', $user->id) ->get();

	 	  return $tasks;
   }
 
   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
   public function store(Request $request) {
	     
        $user = JWTAuth::parseToken()->authenticate();
   
        $task = new Task($request->all());
        $task->user_id = $user->id;
        $task->save();
       
        return $task;
   }

   public function update(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $task = Task::where('user_id', $user->id)->where('id',$id)->first();
 
        if($task){
            $task->title = $request->input('title');
            $task->isDone = $request->input('isDone');
            $task->save();
            return $task;
        }else{
            return response('Unauthorized',403);
        }
    }

    public function destroy($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $task = Task::where('user_id', $user->id)->where('id',$id)->first();

        if($task){
            Task::destroy($task->id);
            return  response('Success',200);;
        }else{
            return response('Unauthorized',403);
        }
    }
}
