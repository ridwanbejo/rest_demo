<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
 
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;

class AuthenticateController extends Controller
{
	
    public function register(Request $request){
 
        $newuser = $request->all();
        $password = bcrypt($request->input('password'));
 
        $newuser['password'] = $password;
 
        return User::create($newuser);
    }
}
